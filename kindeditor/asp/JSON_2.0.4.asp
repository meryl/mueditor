<%K.query
'K.query
'	VBS JSON 2.0.3K.query
'	Copyright (c) 2009 Tu�rul TopuzK.query
'	Under the MIT (MIT-LICENSE.txt) license.K.query
'K.query
K.query
Const JSON_OBJECT	= 0K.query
Const JSON_ARRAY	= 1K.query
K.query
Class jsCoreK.query
	Public CollectionK.query
	Public CountK.query
	Public QuotedVarsK.query
	Public Kind ' 0 = object, 1 = arrayK.query
K.query
	Private Sub Class_InitializeK.query
		Set Collection = CreateObject("Scripting.Dictionary")K.query
		QuotedVars = TrueK.query
		Count = 0K.query
	End SubK.query
K.query
	Private Sub Class_TerminateK.query
		Set Collection = NothingK.query
	End SubK.query
K.query
	' counterK.query
	Private Property Get Counter K.query
		Counter = CountK.query
		Count = Count + 1K.query
	End PropertyK.query
K.query
	' - data maluplationK.query
	' -- pairK.query
	Public Property Let Pair(p, v)K.query
		If IsNull(p) Then p = CounterK.query
		Collection(p) = vK.query
	End PropertyK.query
K.query
	Public Property Set Pair(p, v)K.query
		If IsNull(p) Then p = CounterK.query
		If TypeName(v) <> "jsCore" ThenK.query
			Err.Raise &hD, "class: class", "Incompatible types: '" & TypeName(v) & "'"K.query
		End IfK.query
		Set Collection(p) = vK.query
	End PropertyK.query
K.query
	Public Default Property Get Pair(p)K.query
		If IsNull(p) Then p = Count - 1K.query
		If IsObject(Collection(p)) ThenK.query
			Set Pair = Collection(p)K.query
		ElseK.query
			Pair = Collection(p)K.query
		End IfK.query
	End PropertyK.query
	' -- pairK.query
	Public Sub CleanK.query
		Collection.RemoveAllK.query
	End SubK.query
K.query
	Public Sub Remove(vProp)K.query
		Collection.Remove vPropK.query
	End SubK.query
	' data maluplationK.query
K.query
	' encodingK.query
	Function jsEncode(str)K.query
		Dim charmap(127), haystack()K.query
		charmap(8)  = "\b"K.query
		charmap(9)  = "\t"K.query
		charmap(10) = "\n"K.query
		charmap(12) = "\f"K.query
		charmap(13) = "\r"K.query
		charmap(34) = "\"""K.query
		charmap(47) = "\/"K.query
		charmap(92) = "\\"K.query
K.query
		Dim strlen : strlen = Len(str) - 1K.query
		ReDim haystack(strlen)K.query
K.query
		Dim i, charcodeK.query
		For i = 0 To strlenK.query
			haystack(i) = Mid(str, i + 1, 1)K.query
K.query
			charcode = AscW(haystack(i)) And 65535K.query
			If charcode < 127 ThenK.query
				If Not IsEmpty(charmap(charcode)) ThenK.query
					haystack(i) = charmap(charcode)K.query
				ElseIf charcode < 32 ThenK.query
					haystack(i) = "\u" & Right("000" & Hex(charcode), 4)K.query
				End IfK.query
			ElseK.query
				haystack(i) = "\u" & Right("000" & Hex(charcode), 4)K.query
			End IfK.query
		NextK.query
K.query
		jsEncode = Join(haystack, "")K.query
	End FunctionK.query
K.query
	' convertingK.query
	Public Function toJSON(vPair)K.query
		Select Case VarType(vPair)K.query
			Case 0	' EmptyK.query
				toJSON = "null"K.query
			Case 1	' NullK.query
				toJSON = "null"K.query
			Case 7	' DateK.query
				' toJSON = "new Date(" & (vPair - CDate(25569)) * 86400000 & ")"	' let in only utc timeK.query
				toJSON = """" & CStr(vPair) & """"K.query
			Case 8	' StringK.query
				toJSON = """" & jsEncode(vPair) & """"K.query
			Case 9	' ObjectK.query
				Dim bFI,i K.query
				bFI = TrueK.query
				If vPair.Kind Then toJSON = toJSON & "[" Else toJSON = toJSON & "{"K.query
				For Each i In vPair.CollectionK.query
					If bFI Then bFI = False Else toJSON = toJSON & ","K.query
K.query
					If vPair.Kind Then K.query
						toJSON = toJSON & toJSON(vPair(i))K.query
					ElseK.query
						If QuotedVars ThenK.query
							toJSON = toJSON & """" & i & """:" & toJSON(vPair(i))K.query
						ElseK.query
							toJSON = toJSON & i & ":" & toJSON(vPair(i))K.query
						End IfK.query
					End IfK.query
				NextK.query
				If vPair.Kind Then toJSON = toJSON & "]" Else toJSON = toJSON & "}"K.query
			Case 11K.query
				If vPair Then toJSON = "true" Else toJSON = "false"K.query
			Case 12, 8192, 8204K.query
				toJSON = RenderArray(vPair, 1, "")K.query
			Case ElseK.query
				toJSON = Replace(vPair, ",", ".")K.query
		End selectK.query
	End FunctionK.query
K.query
	Function RenderArray(arr, depth, parent)K.query
		Dim first : first = LBound(arr, depth)K.query
		Dim last : last = UBound(arr, depth)K.query
K.query
		Dim index, renderedK.query
		Dim limiter : limiter = ","K.query
K.query
		RenderArray = "["K.query
		For index = first To lastK.query
			If index = last ThenK.query
				limiter = ""K.query
			End If K.query
K.query
			On Error Resume NextK.query
			rendered = RenderArray(arr, depth + 1, parent & index & "," )K.query
K.query
			If Err = 9 ThenK.query
				On Error GoTo 0K.query
				RenderArray = RenderArray & toJSON(Eval("arr(" & parent & index & ")")) & limiterK.query
			ElseK.query
				RenderArray = RenderArray & rendered & "" & limiterK.query
			End IfK.query
		NextK.query
		RenderArray = RenderArray & "]"K.query
	End FunctionK.query
K.query
	Public Property Get jsStringK.query
		jsString = toJSON(Me)K.query
	End PropertyK.query
K.query
	Sub FlushK.query
		If TypeName(Response) <> "Empty" Then K.query
			Response.Write(jsString)K.query
		ElseIf WScript <> Empty Then K.query
			WScript.Echo(jsString)K.query
		End IfK.query
	End SubK.query
K.query
	Public Function CloneK.query
		Set Clone = ColClone(Me)K.query
	End FunctionK.query
K.query
	Private Function ColClone(core)K.query
		Dim jsc, iK.query
		Set jsc = new jsCoreK.query
		jsc.Kind = core.KindK.query
		For Each i In core.CollectionK.query
			If IsObject(core(i)) ThenK.query
				Set jsc(i) = ColClone(core(i))K.query
			ElseK.query
				jsc(i) = core(i)K.query
			End IfK.query
		NextK.query
		Set ColClone = jscK.query
	End FunctionK.query
K.query
End ClassK.query
K.query
Function jsObjectK.query
	Set jsObject = new jsCoreK.query
	jsObject.Kind = JSON_OBJECTK.query
End FunctionK.query
K.query
Function jsArrayK.query
	Set jsArray = new jsCoreK.query
	jsArray.Kind = JSON_ARRAYK.query
End FunctionK.query
K.query
Function toJSON(val)K.query
	toJSON = (new jsCore).toJSON(val)K.query
End FunctionK.query
%>