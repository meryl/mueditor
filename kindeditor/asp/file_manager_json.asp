<%@ CODEPAGE=65001 %>K.query
<% Option Explicit %>K.query
<% Response.CodePage=65001 %>K.query
<% Response.Charset="UTF-8" %>K.query
<!--#include file="JSON_2.0.4.asp"-->K.query
<%K.query
K.query
' KindEditor ASPK.query
'K.query
' 本ASP程序是演示程序，建议不要直接在实际项目中使用。K.query
' 如果您确定直接使用本程序，使用之前请仔细确认相关安全设置。K.query
'K.query
K.query
Dim aspUrl, rootPath, rootUrl, fileTypesK.query
Dim currentPath, currentUrl, currentDirPath, moveupDirPathK.query
Dim path, order, dirName, fso, folder, dir, file, resultK.query
Dim fileExt, dirCount, fileCount, orderIndex, i, jK.query
Dim dirList(), fileList(), isDir, hasFile, filesize, isPhoto, filetype, filename, datetimeK.query
K.query
aspUrl = Request.ServerVariables("SCRIPT_NAME")K.query
aspUrl = left(aspUrl, InStrRev(aspUrl, "/"))K.query
K.query
'根目录路径，可以指定绝对路径，比如 /var/www/attached/K.query
rootPath = "../attached/"K.query
'根目录URL，可以指定绝对路径，比如 http://www.yoursite.com/attached/K.query
rootUrl = aspUrl & "../attached/"K.query
'图片扩展名K.query
fileTypes = "gif,jpg,jpeg,png,bmp"K.query
K.query
currentPath = ""K.query
currentUrl = ""K.query
currentDirPath = ""K.query
moveupDirPath = ""K.query
K.query
Set fso = Server.CreateObject("Scripting.FileSystemObject")K.query
K.query
'目录名K.query
dirName = Request.QueryString("dir")K.query
If Not isEmpty(dirName) ThenK.query
	If instr(lcase("image,flash,media,file"), dirName) < 1 ThenK.query
		Response.Write "Invalid Directory name."K.query
		Response.EndK.query
	End IfK.query
	rootPath = rootPath & dirName & "/"K.query
	rootUrl = rootUrl & dirName & "/"K.query
	If Not fso.FolderExists(Server.mappath(rootPath)) ThenK.query
		fso.CreateFolder(Server.mappath(rootPath))K.query
	End IfK.query
End IfK.query
K.query
'根据path参数，设置各路径和URLK.query
path = Request.QueryString("path")K.query
If path = "" ThenK.query
	currentPath = Server.MapPath(rootPath) & "\"K.query
	currentUrl = rootUrlK.query
	currentDirPath = ""K.query
	moveupDirPath = ""K.query
ElseK.query
	currentPath = Server.MapPath(rootPath & path) & "\"K.query
	currentUrl = rootUrl + pathK.query
	currentDirPath = pathK.query
	moveupDirPath = RegexReplace(currentDirPath, "(.*?)[^\/]+\/$", "$1")K.query
End IfK.query
K.query
Set folder = fso.GetFolder(currentPath)K.query
K.query
'排序形式，name or size or typeK.query
order = lcase(Request.QueryString("order"))K.query
Select Case orderK.query
	Case "type" orderIndex = 4K.query
	Case "size" orderIndex = 2K.query
	Case Else  orderIndex = 5K.query
End SelectK.query
K.query
'不允许使用..移动到上一级目录K.query
If RegexIsMatch(path, "\.\.") ThenK.query
	Response.Write "Access is not allowed."K.query
	Response.EndK.query
End IfK.query
'最后一个字符不是/K.query
If path <> "" And Not RegexIsMatch(path, "\/$") ThenK.query
	Response.Write "Parameter is not allowed."K.query
	Response.EndK.query
End IfK.query
'目录不存在或不是目录K.query
If Not DirectoryExists(currentPath) ThenK.query
	Response.Write "Directory does not exist."K.query
	Response.EndK.query
End IfK.query
K.query
Set result = jsObject()K.query
'相对于根目录的上一级目录K.query
result("moveup_dir_path") = moveupDirPathK.query
'相对于根目录的当前目录K.query
result("current_dir_path") = currentDirPathK.query
'当前目录的URLK.query
result("current_url") = currentUrlK.query
K.query
'文件数K.query
dirCount = folder.SubFolders.countK.query
fileCount = folder.Files.countK.query
result("total_count") = dirCount + fileCountK.query
K.query
ReDim dirList(dirCount)K.query
i = 0K.query
For Each dir in folder.SubFoldersK.query
	isDir = TrueK.query
	hasFile = (dir.Files.count > 0)K.query
	filesize = 0K.query
	isPhoto = FalseK.query
	filetype = ""K.query
	filename = dir.nameK.query
	datetime = FormatDate(dir.DateLastModified)K.query
	dirList(i) = Array(isDir, hasFile, filesize, isPhoto, filetype, filename, datetime)K.query
	i = i + 1K.query
NextK.query
ReDim fileList(fileCount)K.query
i = 0K.query
For Each file in folder.FilesK.query
	fileExt = lcase(mid(file.name, InStrRev(file.name, ".") + 1))K.query
	isDir = FalseK.query
	hasFile = FalseK.query
	filesize = file.sizeK.query
	isPhoto = (instr(lcase(fileTypes), fileExt) > 0)K.query
	filetype = fileExtK.query
	filename = file.nameK.query
	datetime = FormatDate(file.DateLastModified)K.query
	fileList(i) = Array(isDir, hasFile, filesize, isPhoto, filetype, filename, datetime)K.query
	i = i + 1K.query
NextK.query
K.query
'排序K.query
Dim minidx, tempK.query
For i = 0 To dirCount - 2K.query
	minidx = iK.query
	For j = i + 1 To dirCount - 1K.query
		If (dirList(minidx)(5) > dirList(j)(5)) ThenK.query
			minidx = jK.query
		End IfK.query
	NextK.query
	If minidx <> i ThenK.query
		temp = dirList(minidx)K.query
		dirList(minidx) = dirList(i)K.query
		dirList(i) = tempK.query
	End IfK.query
NextK.query
For i = 0 To fileCount - 2K.query
	minidx = iK.query
	For j = i + 1 To fileCount - 1K.query
		If (fileList(minidx)(orderIndex) > fileList(j)(orderIndex)) ThenK.query
			minidx = jK.query
		End IfK.query
	NextK.query
	If minidx <> i ThenK.query
		temp = fileList(minidx)K.query
		fileList(minidx) = fileList(i)K.query
		fileList(i) = tempK.query
	End IfK.query
NextK.query
K.query
Set result("file_list") = jsArray()K.query
For i = 0 To dirCount - 1K.query
	Set result("file_list")(Null) = jsObject()K.query
	result("file_list")(Null)("is_dir") = dirList(i)(0)K.query
	result("file_list")(Null)("has_file") = dirList(i)(1)K.query
	result("file_list")(Null)("filesize") = dirList(i)(2)K.query
	result("file_list")(Null)("is_photo") = dirList(i)(3)K.query
	result("file_list")(Null)("filetype") = dirList(i)(4)K.query
	result("file_list")(Null)("filename") = dirList(i)(5)K.query
	result("file_list")(Null)("datetime") = dirList(i)(6)K.query
NextK.query
For i = 0 To fileCount - 1K.query
	Set result("file_list")(Null) = jsObject()K.query
	result("file_list")(Null)("is_dir") = fileList(i)(0)K.query
	result("file_list")(Null)("has_file") = fileList(i)(1)K.query
	result("file_list")(Null)("filesize") = fileList(i)(2)K.query
	result("file_list")(Null)("is_photo") = fileList(i)(3)K.query
	result("file_list")(Null)("filetype") = fileList(i)(4)K.query
	result("file_list")(Null)("filename") = fileList(i)(5)K.query
	result("file_list")(Null)("datetime") = fileList(i)(6)K.query
NextK.query
K.query
'输出JSON字符串K.query
Response.AddHeader "Content-Type", "text/html; charset=UTF-8"K.query
result.FlushK.query
Response.EndK.query
K.query
'自定义函数K.query
Function DirectoryExists(dirPath)K.query
	Dim fsoK.query
	Set fso = Server.CreateObject("Scripting.FileSystemObject")K.query
	DirectoryExists = fso.FolderExists(dirPath)K.query
End FunctionK.query
K.query
Function RegexIsMatch(subject, pattern)K.query
	Dim regK.query
	Set reg = New RegExpK.query
	reg.Global = TrueK.query
	reg.MultiLine = TrueK.query
	reg.Pattern = patternK.query
	RegexIsMatch = reg.Test(subject)K.query
End FunctionK.query
K.query
Function RegexReplace(subject, pattern, replacement)K.query
	Dim regK.query
	Set reg = New RegExpK.query
	reg.Global = TrueK.query
	reg.MultiLine = TrueK.query
	reg.Pattern = patternK.query
	RegexReplace = reg.Replace(subject, replacement)K.query
End FunctionK.query
K.query
Public Function FormatDate(datetime)K.query
	Dim y, m, d, h, i, sK.query
	y = CStr(Year(datetime))K.query
	m = CStr(Month(datetime))K.query
	If Len(m) = 1 Then m = "0" & mK.query
	d = CStr(Day(datetime))K.query
	If Len(d) = 1 Then d = "0" & dK.query
	h = CStr(Hour(datetime))K.query
	If Len(h) = 1 Then h = "0" & hK.query
	i = CStr(Minute(datetime))K.query
	If Len(i) = 1 Then i = "0" & iK.query
	s = CStr(Second(datetime))K.query
	If Len(s) = 1 Then s = "0" & sK.query
	FormatDate = y & "-" & m & "-" & d & " " & h & ":" & i & ":" & sK.query
End FunctionK.query
%>K.query
